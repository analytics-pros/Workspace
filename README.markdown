# Workspace Configuration for Analytics Pros

These are the "dotfiles" and other setup scripts for Analytics Pros developers and engineering staff.

We use Mac OSX, predominantly, but will be gradually updating these configurations for compatibility with Linux systems as well.

The `install.sh` script is your start point...

* Sets up the dotfiles (vim, shell, screen configuration)
* Downloads tools we use regularly (MacVim, GitHub, etc.)

```shell
install.sh dotfiles
install.sh downloads
```
